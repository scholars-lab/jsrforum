# jsrforum.lib.virginia.edu

This repo was created as a process of moving the site from the old SDS servers and into an 'archived' state on Library IT servers.

This repo contains a direct copy of all the files from the SDS server, minus the .svn files and the .htaccess file, and the inclusion of this README file.
